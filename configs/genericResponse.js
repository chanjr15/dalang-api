
exports.OK = (res, data) => {
    const jsonData = {
        status: 'SUCCESS',
        message: data.message,
        data: data.data,
    };
    res.status(200).json(jsonData);
    res.end();
}

exports.NOT_OK = (res, err) => {
    var statusCode = 500;
    var jsonData = {
        status : 'ERROR',
        error : err,
    }
    switch (true) {
        case (typeof (err) === 'string'):
            statusCode = 400;
            jsonData.status = 'FAIL';
            break;
        case (err.name === 'UnauthorizedError'):
            statusCode = 401;
            jsonData.status = 'FAIL';
            jsonData.error = err.name;
            break;
        default:
            break;
    }
    res.status(statusCode).json(jsonData);
    res.end();
}