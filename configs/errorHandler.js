const response = require('./genericResponse');
module.exports = errorHandler;

function errorHandler(err, req, res, next) {
    return response.NOT_OK(res, err);
}