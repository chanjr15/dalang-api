const expressJwt = require('express-jwt');

module.exports = jwt;

function jwt() {
    const secret = process.env.SECRET_JWT;
    return expressJwt({ secret }).unless({
        path: [
            '/api/v1/users/sendOtpRegister',
            '/api/v1/users/registerUser',
            '/api/v1/users/loginUser',
            '/api/v1/users/sendOtpLogin',
            '/api/v1/users/getSingleUser',
            '/api/v1/users/register',
            '/api/v1/users/token',
        ]
    });
}