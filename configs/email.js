
exports.send = async (message) => {
    const mail = require('nodemailer');
    let transport =  mail.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS,
        }
    });

    let info = await transport.sendMail(message);

}