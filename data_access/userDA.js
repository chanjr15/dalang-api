var pool = require('../configs/dbConn');

async function deleteExistOtp(otp_holder) {
    try {
        await pool.query("delete from mst_otp where ?", { otp_holder });
    } catch (error) {
        throw error;
    }
};

async function getExistOtp(otp_holder) {
    try {
        var result = await pool.query("select * from mst_otp where ?", { otp_holder });
        return result[0][0];
    } catch (error) {
        throw error;
    }
}

async function getSingleUser(account_id) {
    try {
        var result = await pool.query("select * from mst_account where ?", { account_id });
        return result[0][0];
    } catch (error) {
        throw error;
    }
};

async function insertOtp(param) {
    try {
        var result = await pool.query("insert into mst_otp set ? ", param);
        return result[0].affectedRows;
    } catch (error) {
        throw error;
    }
}

async function createUser(param) {
    const conn = await pool.getConnection();
    await conn.beginTransaction();
    try {
        await conn.query('insert into mst_user set ?', param.user);
        await conn.query('insert into mst_account set ?', param.auth);
        await conn.commit();
    } catch (error) {
        await conn.rollback();
        throw error;
    } finally {
        conn.release();
    }
}

async function updateUser(params,user_unique_id) {
    try {
        await pool.query("update mst_user set ?  where user_unique_id = ?", [params, user_unique_id]);
    } catch (error) {
        throw error;
    }
}

module.exports = {
    getSingleUser,
    insertOtp,
    deleteExistOtp,
    getExistOtp,
    createUser,
    updateUser
}