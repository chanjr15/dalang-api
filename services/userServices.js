const userDA = require("../data_access/userDA");
const bcrypt = require("bcryptjs");
const email = require("../configs/email");
const jwt = require("jsonwebtoken");

exports.getSingleUser = async auth_id => {
  const user = await userDA.getSingleUser(auth_id);
  if (!user) {
    throw "USER_NOT_EXIST";
  }
  return {
    message: "USER_EXIST",
    data: true
  };
}

exports.register = async params => {
  try {

    const user = await userDA.getSingleUser(params.auth_id);
    if (user) {
      throw "EXIST_USER";
    } else if (!params.password) {
      throw "PASS_REQUIRED";
    }

    const hassPass = bcrypt.hashSync(params.password, 6);
    const currDate = new Date();
    const userUniquId = "USER@" + params.auth_id;
    const token = jwt.sign({ sub: params.auth_id }, process.env.SECRET_JWT);
    const mst_user = {
      user_unique_id: userUniquId,
      user_full_name: params.user_full_name,
      user_email: params.email,
      created_by: params.auth_id,
      user_mobile_no: params.mobile_no,
      created_date: currDate
    };
    const mst_account = {
      account_id: params.auth_id,
      account_credential_hash: hassPass,
      user_unique_id: userUniquId,
      created_by: params.auth_id,
      created_date: currDate
    };
    await userDA.createUser({ user: mst_user, auth: mst_account });
    return {
      message: "USER_CREATED",
      data: {
        token: token
      }
    };
  } catch (error) {
    throw error;
  }
}

exports.token = async auth_id => {
  const token = jwt.sign({ sub: auth_id }, process.env.SECRET_JWT);
  return {
    message: "TOKEN_CREATED",
    data: {
      token: token
    }
  };

}

exports.registerOtp = async auth_id => {
  //check user exist
  const user = await userDA.getSingleUser(auth_id);
  if (user) {
    throw "USER_EXIST";
  }
  const response = await sendOtp(auth_id);
  return {
    message: "OTP_SENT",
    data: response
  };
};

exports.loginOtp = async auth_id => {
  //check user does not exist
  const user = await userDA.getSingleUser(auth_id);

  if (!user) {
    throw "USER_NOT_EXIST";
  }
  const expired = await sendOtp(auth_id);
  return {
    message: "OTP_SENT",
    data: {
      exipries_in: expired
    }
  };
};

sendOtp = async auth_id => {
  //delete Otp if exist using for resendOtp too
  await userDA.deleteExistOtp(auth_id);
  //generateOtp
  const otp = require("../utils/generateOtp").randomString(6);

  //create OTP hash
  const otpHash = bcrypt.hashSync(otp, 6);

  //insert to mst_otp
  const ExpireDate = new Date();
  ExpireDate.setMinutes(ExpireDate.getMinutes() + 5); //parameterize;
  const currentDate = new Date();
  const mst_otp = {
    otp_holder: auth_id,
    otp_hash: otpHash,
    otp_expired: ExpireDate,
    created_by: auth_id,
    created_date: currentDate
  };
  const insertOtp = await userDA.insertOtp(mst_otp);

  if (insertOtp > 0) {
    const message = {
      from: process.env.EMAIL_USER,
      to: auth_id,
      subject: "Otp Verification",
      text: "Your OTP is : " + otp
    };
    email.send(message);
  }
  return {
    expired: ExpireDate,
    otp: otp
  };
};

validateOtp = async params => {
  const storedOtp = await userDA.getExistOtp(params.auth_id);
  if (!storedOtp) throw "OTP_EXPIRED";
  const isExpired = new Date() - new Date(storedOtp.otp_expired) > 300000; //parameterized
  if (isExpired) throw "OTP_EXPIRED";
  else if (!bcrypt.compareSync(params.otp, storedOtp.otp_hash))
    throw "OTP_INVALID";
};

exports.registerUser = async params => {
  try {
    await validateOtp(params);

    const user = await userDA.getSingleUser(params.auth_id);
    if (user) {
      throw "EXIST_USER";
    } else if (!params.password) {
      throw "PASS_REQUIRED";
    }

    const hassPass = bcrypt.hashSync(params.password, 6);
    const currDate = new Date();
    const userUniquId = "USER@" + params.auth_id;
    const token = jwt.sign({ sub: params.auth_id }, process.env.SECRET_JWT);
    const mst_user = {
      user_unique_id: userUniquId,
      user_full_name: params.user_full_name,
      user_email: params.email,
      created_by: params.auth_id,
      user_mobile_no: params.mobile_no,
      created_date: currDate
    };
    const mst_account = {
      account_id: params.auth_id,
      account_credential_hash: hassPass,
      user_unique_id: userUniquId,
      created_by: params.auth_id,
      created_date: currDate
    };
    await userDA.createUser({ user: mst_user, auth: mst_account });
    return {
      message: "USER_CREATED",
      data: {
        token: token
      }
    };
  } catch (error) {
    throw error;
  }
};

exports.loginUser = async params => {
  try {
    const user = await userDA.getSingleUser(params.auth_id);
    if (!user) {
      throw "USER_NOT_FOUND";
    } else if (!params.password) {
      throw "PASS_REQUIRED";
    }

    if (!bcrypt.compareSync(params.password, user.account_credential_hash)) {
      throw "PASS_WRONG";
    }

    const token = jwt.sign({ sub: params.auth_id }, process.env.SECRET_JWT);

    return {
      message: "USER_VALID",
      data: {
        token: token
      }
    };
  } catch (error) {
    throw error;
  }
};

exports.updateUser = async params => {
  try {
    const user = await userDA.getSingleUser(params.auth_id);
    if (!user) {
      throw "USER_NOT_FOUND";
    }
    const mst_user = {
      user_id_card_no: params.user_id_card_no,
      user_full_name: params.user_full_name,
      user_mobile_no: params.user_mobile_no,
      user_street_name: params.user_street_name,
      modified_by: params.auth_id,
      modified_date: new Date()
    };
    await userDA.updateUser(mst_user, user.user_unique_id);

    return {
      message: "USER_UPDATED",
      data: {}
    };
  } catch (error) {
    throw error;
  }
};
