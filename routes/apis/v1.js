const UserController = require('../../controllers/userController');

const express = require('express');
let router = express.Router();
router.use('/users',UserController);
module.exports = router;
