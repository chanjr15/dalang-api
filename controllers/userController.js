const express = require('express');
const userService = require('../services/userServices');
// const response = require('../../configs/res');
var HttpStatus = require('http-status-code');
const response = require('../configs/genericResponse');

let router = express.Router();

router.post('/sendOtpRegister', registerOtp);
router.post('/registerUser', registerUser);
router.post('/loginUser', loginUser);
router.post('/sendOtpLogin', loginOtp);
router.post('/updateUser', updateUser);
router.post('/getSingleUser', getSingleUser);
router.post('/register', register);
router.post('/token', getToken);

async function getSingleUser(req, res, next) {
    try {
        const result = await userService.getSingleUser(req.body.auth_id);
        response.OK(res, result);
    } catch (err) {
        response.NOT_OK(res, err);
    }
}

async function register(req, res, next) {
    try {
        const result = await userService.register(req.body);
        response.OK(res, result);
    } catch (err) {
        response.NOT_OK(res, err);
    }
}

async function getToken(req, res, next) {
    try {
        const result = await userService.token(req.body.auth_id);
        response.OK(res, result);
    } catch (error) {
        response.NOT_OK(res, err);
    }
}

async function registerOtp(req, res, next) {
    try {
        const result = await userService.registerOtp(req.body.auth_id);
        response.OK(res, result);
    } catch (err) {
        response.NOT_OK(res, err);
    }
}

async function registerUser(req, res, next) {
    try {
        var result = await userService.registerUser(req.body);
        response.OK(res, result);
    } catch (error) {
        response.NOT_OK(res, error);
    }
}

async function loginUser(req, res, next) {
    try {
        var result = await userService.loginUser(req.body);
        response.OK(res, result);
    } catch (error) {
        response.NOT_OK(res, error);
    }
}

async function loginOtp(req, res, next) {
    try {
        var result = await userService.loginOtp(req.body.auth_id);
        response.OK(res, result);
    } catch (error) {
        response.NOT_OK(res, error);
    }
}

async function updateUser(req, res, next) {
    try {
        var result = await userService.updateUser(req.body);
        response.OK(res, result);
    } catch (error) {
        response.NOT_OK(res, error);
    }
}

module.exports = router;
