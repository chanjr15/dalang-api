const server = require('./configs/app')();
require('dotenv').config();

server.create(process.env);

server.start();
